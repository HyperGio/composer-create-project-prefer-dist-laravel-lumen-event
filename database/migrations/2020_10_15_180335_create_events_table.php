<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table -> string("nome");
            $table -> string("cover_url")->nulltable();
            $table -> decimal("prezzo");
            $table -> string("indirizzo");
            $table -> decimal('lat',11,8);
            $table -> decimal('lng',11,8);
            $table -> integer('numero_visualizzazioni')->nulltable();
            $table -> integer('numero_commenti')->nulltable();
            $table -> integer('numero_like')->nulltable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
